<?php get_header() ?>
<div class="container" style="padding:30px 5%;">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<? get_the_title() ?>
<?php the_content() ?>

<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
</div>

<?php get_footer() ?>
