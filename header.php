<!doctype html>
<html lang="es">
  <head>

    <title><?php if ( is_front_page( ) ) { echo __( 'Galería Colombia: Fotos, Arte Digital, Experiencias', 'gettext' );} else {echo the_title();echo " | Galería Colombia";} ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:title" content="Galería Colombia" />
    <meta property="og:image" content="<?php echo get_template_directory_uri() ?>/img/logo-redes.jpg"/>
    <meta property="og:description" content="" />
    <meta property="og:url" content="https://www.galeriacolombia.com" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/favorites.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var favorites_data = {
  "ajaxurl":"https:\/\/www.galeriacolombia.com\/wp-admin\/admin-ajax.php",
  "nonce":"6b4276b954",
  "favorite":"<div class=\"gcempty\"></div>",
  "favorited":"<div class=\"gcfull\"></div>",
  "includecount":"",
  "indicate_loading":"",
  "loading_text":"",
  "loading_image":"<span class=\"icon-spinner-wrapper\"><i class=\"icon-spinner\"><\/i><\/span>",
  "loading_image_active":"<span class=\"icon-spinner-wrapper\"><i class=\"icon-spinner\"><\/i><\/span>",
  "loading_image_preload":"",
  "cache_enabled":"1",
  "button_options":{
        "button_type":"custom",
        "custom_colors":false,
        "box_shadow":false,
        "include_count":false,
        "default":{
          },
        "active":{
          }
      },
  "authentication_modal_content":"<p>Please login to add favorites.<\/p><p><a href=\"#\" data-favorites-modal-close>Dismiss this notice<\/a><\/p>",
  "authentication_redirect":"",
  "dev_mode":"",
  "logged_in":"",
  "user_id":"0",
  "authentication_redirect_url":"http:\/\/alejandro.local/~MacDeAlejo/galeriacolombia-v2.0\/wp-login.php"};
/* ]]> */
</script>


  </head>
  <body>


     <style media="screen">

       body{background: #fff}
       h1, h2, h3, h6 {color:#444444}
     .container{opacity:0/*background: #fff; border:1px solid #ccc; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px;border-top: 0;*/}
   .nav-pantalla{background-color:#fbfbfb; border-bottom:1px solid #ddd; width:100%; max-width:1440px; margin: 0 auto;padding: 15px 20px;position: relative;/*position:relative;z-index:2;box-shadow: 0 0px 5px 0 rgba(0, 0, 0, 0.10);*/}
        .nav-home{border-bottom:0;margin: 0 auto -66px;background-color: rgba(255,255,255,0.5); opacity: 0;z-index:2; -webkit-transition: opacity 200ms; /* Safari */transition: opacity 200ms;}
       .nav-screen{width: 100%}
       .foto-cabezote{width:100%; height:auto; border:1px solid #ddd}
       .container-t div{background: rgba(255,255,255,0.9); padding: 20px; display:inline-block}
       .container-t {text-align: center; }
       .container-p{
         padding: 50px 15% 50px;

       }
       .container-pico{
         width: 100%;
         margin: 0 auto;
         max-width: 1440px;
         min-height: 450px;
         padding: 80px 5% 0;
         background-size: cover;
         background-position: center;
         background-repeat: no-repeat;
       }
       .container-f{
         opacity:0;
         width: 100%;
         margin: 0 auto;
         max-width: 1440px;
         min-height: 700px;
         padding: 240px 5% 0;
         background-size: cover;
         background-position: center;
         background-repeat: no-repeat;
       }
       .firma-div a:hover{text-decoration: underline;}
       .loaded{
         opacity: 1 !important;
         -webkit-transition: opacity 200ms; /* Safari */
         transition: opacity 200ms;
       }
       .hidden{display: none;}
       .glyphicon-refresh-animate {
             -animation: spin .7s infinite linear;
             -webkit-animation: spin2 .7s infinite linear;
         }
         @-webkit-keyframes spin2 {
             from { -webkit-transform: rotate(0deg);}
             to { -webkit-transform: rotate(360deg);}
         }
         @keyframes spin {
             from { transform: scale(1) rotate(0deg);}
             to { transform: scale(1) rotate(360deg);}
         }
         .cart-contents:before {
    font-family:WooCommerce;
    content: "\e01d";
    font-size:28px;
    margin-top:10px;
    font-style:normal;
    font-weight:400;
    padding-right:5px;
    vertical-align: bottom;
}
.cart-contents:hover {
    text-decoration: none;
}
.cart-contents-count {
    color: #fff;
    background-color: #2ecc71;
    font-weight: bold;
    border-radius: 10px;
    padding: 1px 6px;
    line-height: 1;
    font-family: Arial, Helvetica, sans-serif;
    vertical-align: top;
}
.quantity{display: inline-block;}
input.qty{width: 70px; display: inline;}
.attachment-shop_single{width:100%; height:auto;}
.tab-content{padding: 30px 20px;min-height: 250px;}
#rating{width: 200px}
#comment{max-width: 550px}
.products li{display: inline-block; width: 30%}
.products li a img{width: 100%; height:auto}
.woocommerce-loop-product__link h6{color:#444444; text-align: center;}
.product .add_to_cart_button{display: none;}
.woocommerce-loop-product__link:hover{color :#444444;}
.woocommerce-product-gallery__image a img{width:100%; height:auto;}
.foto-flickr{display: inline-block; width:24%; margin:1px}
.foto-flickr a img{width: 100%; height: auto;}
.imagen-full{width:100%;height:auto}
.product-thumbnail a img{width: 100%; height:auto}
.form-cupones{text-align: right;}
.cart-collaterals{text-align: -webkit-right;}
.woocommerce-form__input-checkbox{margin-left: 10px}
.topmenu-div{padding-top: 5px}
.topmenu-div a{color:#444; padding-left: 10px}
.topmenu-div a:hover{color:#444}
.woocommerce{padding: 30px 5%;}
:focus {
outline: 0;
}
.price{font-weight: 600;font-size: 24px;color: #777;}
#image-variacion img:focus{outline: none}
#image-variacion:focus{outline: none}
.product-remove{vertical-align: middle !important}
.product-remove a{color: red; font-size: 20px}
.comunidad-texto-div{margin-top:50px}
.tienda-texto-div{margin-top:0px; }
.comunidad-div{margin-bottom:50px}
.tienda-div{margin-bottom:50px}
.menu-button-div{
  display: none;
}
.logo-div{
    display: block;
  }
.topmenu-div{
  float:right;
}
.show-mobile{display: none}
.menu-movil{
  padding: 20px 20px;
    position: absolute;
    background: rgba(255,255,255,0.9);
    width: 100%;
    z-index: 2;
    display:none;
}
.menu-movil ul{list-style: none; padding: 0}
.menu-movil ul li{
  border-bottom: 1px solid #eee;
  text-align: center;
  padding: 10px 0;
  font-size: 18px;
}
.menu-movil ul li a{color: #444}
.link-carrito .badge{}
#menu-hamburguesa{cursor: pointer;}
.close-menu{float:right; color:#444;}
.close-menu:hover{float:right; color:#aaa;}
.tienda-div-container{min-height: 580px;padding:60px 5% 30px; width: 100%; max-width: 1440px; background-image: url('<?php echo get_template_directory_uri() ?>/img/fondo-galeria-home.jpg'); background-repeat:no-repeat; background-position:bottom;}
.contenido-producto{padding-right: 0px; padding-left: 0px;}
.contenido-producto h6{font-size:0.9em}

@media (max-width: 991px) {
.container-f{min-height: 620px}
.comunidad-texto-div{margin-top:0px; margin-bottom:50px}
.tienda-texto-div{margin-top:20px; margin-bottom:30px}
.comunidad-div{margin-bottom:0px}
.tienda-div{margin-bottom:0px}
}

@media (max-width: 767px) {
.tienda-div-container{min-height: 900px; padding:60px 10% 30px; }
  .menu-button-div{
    display: inline-block;
    width: 19%;
  }
  .logo-div{
      display: inline-block;
      width: 60%;
      text-align: center;
    }
  .topmenu-div{
    float:right;
    width: 19%;
    text-align: right;
  }
  .hide-mobile{display: none}
  .show-mobile{display: inline-block}
 }



     </style>
            <div class="nav-pantalla <?php if ( is_home() ) { echo "nav-home"; } ?> ">
         <div class="menu-button-div">
           <a id="menu-hamburguesa"><i class="fa fa-bars fa-lg" aria-hidden="true"></i></a>
         </div>
         <div class="logo-div" style="display: inline-block;">
           <a href="<?php echo get_home_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/img/logo-galeria-colombia.png" alt=""></a>
         </div>
         <div class="topmenu-div" style="">
           <a class="hide-mobile" href="<?php echo get_home_url() ?>/?page_id=74">Comunidad</a>
           <a class="hide-mobile" href="<?php echo get_home_url() ?>/?page_id=82">Experiencias</a>
           <a class="hide-mobile" href="<?php echo get_home_url() ?>/?page_id=4">Tienda</a>
           <a class="link-carrito" style="color: #000;" href="<?php echo get_home_url() ?>/?page_id=5"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="badge badge-dark" style="margin-left:3px;font-size: 10px;color: #000;background-color: #ddd;"><?php $count = WC()->cart->cart_contents_count; echo $count; ?></span></a>


<!--
           <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

               $count = WC()->cart->cart_contents_count;
               ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
               if ( $count > 0 ) {
                   ?>
                   <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
                   <?php
               }
                   ?></a>

           <?php } ?>
         -->
         </div>

       </div>
       <div id="menu-movil" class="menu-movil">
         <a id="close-menu" class="close-menu" ><i class="fa fa-times fa-lg"></i></a>
         <br clear="all">
          <ul>
            <li><a href="<?php echo get_home_url() ?>/?page_id=74">Comunidad</a></li>
            <li><a href="<?php echo get_home_url() ?>/?page_id=82">Experiencias</a></li>
            <li><a href="<?php echo get_home_url() ?>/?page_id=4">Tienda</a></li>
            <li><a href="<?php echo get_home_url() ?>/?page_id=5">Carrito</a></li>
          </ul>
       </div>
       </div>
