
<div class="firma-div" style="background-color:#f9f9f9; border-top:1px solid #ccc; padding:20px 5% 30px; margin-top:0px;">
  <p style="float:left; font-size:10px; color:#444; margin:0">© 2018 Galería Colombia. Todos los derechos reservados.</p>
<!--  <a href="https://www.mobitech.co" target="_blank"> <p style="float:right; font-size:10px; color:#444">Mobitech</p></a>-->
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<!--    <script type="text/javascript" src="https://favoriteposts.com/wp-content/plugins/favorites/assets/js/favorites.min.js?ver=2.1.6"></script> -->

   <script src="<?php echo get_template_directory_uri() ?>/js/favorites.min.js"></script>
   <script src="<?php echo get_template_directory_uri() ?>/js/share.js"></script>
<!--    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.usp.core.js"></script>-->

    <script type="text/javascript">
    var buscar = "<?php echo $_GET['buscar'] ?>";
  //  alert(buscar);

  //https://api.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=d41b5fdac033592f420af8d8e4804272&group_id=1358024%40N21&extras=geo%2C+tags&per_page=500&page=1&format=json&nojsoncallback=1


    var $ = jQuery.noConflict();
    $(document).ready(function() {
      $('#usp_add-another').click(function(event) {
    		event.preventDefault();
    		var $this = jQuery(this);
    		var $newInput = $this.parent().find('input:visible:last').clone().val('');
    		$this.before($newInput);
    	});

      setTimeout(function(){
      $('#cargando-text').addClass('hidden');
      $('.container').addClass('loaded');
      $('.woocommerce-product-gallery').css('opacity', '1');
      $('#container-f').addClass('loaded');
      $('#container2').addClass('loaded');
      $('#myCarousel').addClass('loaded');
    }, 800);
    setTimeout(function(){
      $('.nav-home').css('opacity', '1');
    }, 1000);

    $('#tab-product a:first').tab('show')
      $('#tab-product a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  });
  $('.input-text').addClass('form-control');
  $('#pa_tipo-de-marco').addClass('form-control');
  $('.country_select').addClass('form-control');
  $('.button').addClass('btn btn-primary');
  $('.woocommerce-info').addClass('alert alert-secondary');
  $('#menu-hamburguesa').on('click', function (e) {
    $('#menu-movil').css("display", "block")
  });
  $('#close-menu').on('click', function (e) {
    $('#menu-movil').css("display", "none")
  });
  $('.btn-enlace').on('click', function (e) {
    e.preventDefault();
    var direccion = $(this).attr('href');
    var direccion_blank = $(this).attr('target');
//    $(this).tab('show')
//  location.href = direccion;
//  alert(direccion_blank);
  if (direccion_blank=="_blank") {
    window.open(direccion,'_blank')
  } else {
    location.href = direccion;
  }
    });



$('[data-fancybox]').fancybox({
  buttons : ['close']
});
	$("[comunidad-flickr]").fancybox({
    });
  $("[historia]").fancybox({
      });
    $( "#variations_form" ).change(function() {
//        var direccion = document.getElementById('variations_form').attr('data-product_variations').value;
        var color_variacion = $("#pa_tipo-de-marco").val();
        var variaciones = JSON.parse($(this).attr('data-product_variations'));
        var contador_variaciones = variaciones.length;
        for (i = 0; i < contador_variaciones; i++) {
          if(color_variacion==variaciones[i]['attributes']['attribute_pa_tipo-de-marco']){
            var url_imagen = variaciones[i]['image']['src'];
            var url_link = variaciones[i]['image']['full_src'];
            var imagen1 = '<?php echo get_the_post_thumbnail_url( url_imagen, 'medium'); ?>';
            $( "#imagen-variacion" ).find( "img" ).attr('srcset', url_imagen);
            $( "#imagen-variacion" ).attr('href', url_link);
//            console.log(color_variacion);
//            console.log(url_imagen);
          }
        }

        console.log(variaciones);
//        document.getElementById("imagen-variable").append( imagen1 );


    });
  });

  function sumar_personas(){
    var cant_personas = Number(document.getElementById("personas").value);
    document.getElementById("personas").value = cant_personas + 1;
  }
  function restar_personas(){
    if (document.getElementById("personas").value>1){
    var cant_personas = Number(document.getElementById("personas").value);
    document.getElementById("personas").value = cant_personas - 1;
    }
  }
    </script>

<?php if ( is_home() ) {
?>
<script type="text/javascript">
var url_fotos = 'https://api.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=d41b5fdac033592f420af8d8e4804272&group_id=1358024%40N21&format=json&nojsoncallback=1';
//    var url_fotos = 'https://api.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=d41b5fdac033592f420af8d8e4804272&group_id=1358024%40N21&format=json&nojsoncallback=1&auth_token=72157663033815868-ba7874475d03882e&api_sig=8b4e8497063089a1deb1506e7e324712';
$.getJSON(url_fotos, function(jd) {
//      console.log(Object.keys(jd.photos.photo).length);
//      console.log(jd.photos.photo);
//      console.log('https://farm'+jd.photos.photo[0]["farm"]+'.staticflickr.com/'+jd.photos.photo[0]["server"]+'/'+jd.photos.photo[0]["id"]+'_'+jd.photos.photo[0]["secret"]);
  contador_fotos = 50;
  for (i = 0; i < contador_fotos; i++) {
  var url_foto = 'https://farm'+jd.photos.photo[i]["farm"]+'.staticflickr.com/'+jd.photos.photo[i]["server"]+'/'+jd.photos.photo[i]["id"]+'_'+jd.photos.photo[i]["secret"]  ;
  var titulo_foto = jd.photos.photo[i]["title"];
  var owner_foto = jd.photos.photo[i]["ownername"];
  if (i < 12) {
    var html_foto = '<div class="foto-flickr"><a data-fancybox="comunidad-flickr" data-caption="<strong>'+titulo_foto+'</strong><br>Por '+owner_foto+'" href="'+url_foto+'_b.jpg"><img src="'+url_foto+'_q.jpg" alt="'+titulo_foto+'"></a></div>';$( ".fotos-flickr" ).append( html_foto );
} else {
  var html_foto = '<a data-fancybox="comunidad-flickr" data-caption="<strong>'+titulo_foto+'</strong><br>Por '+owner_foto+'" alt="'+titulo_foto+'" href="'+url_foto+'_b.jpg"></a>';$( ".fotos-flickr" ).append( html_foto );
}

}
});



</script>
<?php } ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92907010-7"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-92907010-7');
    </script>

  </body>
</html>
